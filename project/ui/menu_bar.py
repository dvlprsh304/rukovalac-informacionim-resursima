from PySide2 import QtCore, QtWidgets, QtGui

class MenuBar(QtWidgets.QMenuBar):
    def __init__(self, parent):
        super().__init__(parent)
        self.main_window = parent

        #instanciranje Menu opcija
        file_menu = QtWidgets.QMenu("&File", self)
        edit_menu = QtWidgets.QMenu("&Edit", self)
        view_menu = QtWidgets.QMenu("&View", self)
        help_menu = QtWidgets.QMenu("&Help", self)

        #File Actions
        open_file_action = QtWidgets.QAction("&Open File", self,
                shortcut=QtGui.QKeySequence.Open,
                statusTip="Open", triggered=self.main_window.open_new_file_button)

        file_menu.addAction(open_file_action)


        #populate MenuBar
        self.addMenu(file_menu)
        self.addMenu(edit_menu)
        self.addMenu(view_menu)
        self.addMenu(help_menu)


