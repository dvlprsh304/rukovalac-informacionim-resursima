from PySide2 import QtCore, QtWidgets, QtGui
from ui.menu_bar import MenuBar
from ui.tool_bar import ToolBar
from ui.structure_dock import StructureDock
from ui.workspace import Workspace
from handlers.serial_file_handler import SerialFileHandler
from handlers.sequential_file_handler import SequentialFileHandler
import json

class MainWindow(QtWidgets.QMainWindow):
    def __init__(self):
        super(MainWindow, self).__init__()

        self.resize(640, 480)
        self.setWindowTitle("Editor generickih podataka")
        self.setWindowIcon(QtGui.QIcon("ui/icons/edit-file.png"))

        self.menu_bar = MenuBar(self)
        self.tool_bar = ToolBar(self)
        self.central_widget = QtWidgets.QTabWidget(self)
        self.workspace = Workspace(self.central_widget)
        self.status_bar = QtWidgets.QStatusBar(self)
        self.status_bar.showMessage("Prikazan status bar!")
        self.stucture_dock = StructureDock("Structure Dock", self)

        self.central_widget.addTab(self.workspace, QtGui.QIcon("ui/icons/edit-file.png"), "Prikaz tabele")
        self.central_widget.setTabsClosable(True)

        self.setMenuBar(self.menu_bar)
        self.addToolBar(self.tool_bar)
        self.setCentralWidget(self.central_widget)
        self.setStatusBar(self.status_bar)
        self.addDockWidget(QtCore.Qt.LeftDockWidgetArea, self.stucture_dock)

    def open_new_file_button(self):
        path_to_data_file, _ = QtWidgets.QFileDialog.getOpenFileName(self, self.tr("Open Data File"), self.tr("~/Desktop/"))
        print(path_to_data_file)
        self.open_new_file(path_to_data_file)

    def open_new_file(self, filepath):
        meta_data_file_path = filepath.replace("_data.csv", "_metadata.json")
        with open(meta_data_file_path) as meta:
            metadata = json.load(meta)
        file_handler = self.get_file_handler(metadata['type'], filepath, meta_data_file_path)
        
        linked_files = metadata['linked_files']
        if len(linked_files) == 0:
            print("No linked files")
        else:
            linked_path = filepath[:filepath.rindex("/")]
            linked_data_file_path = linked_path + "/" + linked_files[0]
            linked_meta_data_file_path = linked_data_file_path.replace("_data.csv", "_metadata.json")
            with open(linked_meta_data_file_path) as linked_meta:
                linked_meta = json.load(linked_meta)
            
            linked_file_handler = self.get_file_handler(linked_meta['type'], linked_data_file_path, linked_meta_data_file_path)
            self.workspace.refresh_subtable(linked_file_handler)
        
        self.workspace.refresh_table(file_handler)

    def get_file_handler(self, file_type, data_file_path, meta_data_file_path): 
        print(file_type)
        if file_type == "serijska":
            return SerialFileHandler(data_file_path, meta_data_file_path)
        elif file_type == "sekvencijalna":
            return SequentialFileHandler(data_file_path, meta_data_file_path)
        else: 
            return None

    def save(self):
        self.workspace.save()

    