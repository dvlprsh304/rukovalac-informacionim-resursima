from PySide2 import QtWidgets, QtGui, QtCore
from model.generic_model import GenericModel
from model import *

class Workspace(QtWidgets.QWidget):
    def __init__(self, parent):
        super().__init__(parent)
        self.main_layout = QtWidgets.QVBoxLayout()
        self.tab_widget = None
        self.create_tab_widget()
        self.file_handler = None

        self.main_table = QtWidgets.QTableView(self.tab_widget)
        self.main_table.setSelectionMode(QtWidgets.QAbstractItemView.SingleSelection)
        self.main_table.setSelectionBehavior(QtWidgets.QAbstractItemView.SelectRows)
        self.main_table.setModel(None)
        self.main_table.clicked.connect(self.element_selected)
        self.main_table.clicked.connect(self.show_tabs)

        self.subtable = QtWidgets.QTableView(self.tab_widget)
        self.subtable.setSelectionMode(QtWidgets.QAbstractItemView.SingleSelection)
        self.subtable.setSelectionBehavior(QtWidgets.QAbstractItemView.SelectRows)

        self.tab_widget.addTab(self.subtable, QtGui.QIcon("ui/icons/save-file.png"), "Povezana Tabela")
        self.main_layout.addWidget(self.main_table)
        self.main_layout.addWidget(self.tab_widget)
        self.setLayout(self.main_layout)

    def element_selected(self, index):
        model = self.main_table.model()
        selected_element = model.get_element(index)

    def create_tab_widget(self):
        self.tab_widget = QtWidgets.QTabWidget(self)
        self.tab_widget.setTabsClosable(True)

    def show_tabs(self):
        self.tab_widget.addTab(self.subtable, QtGui.QIcon("icons8-edit-file-64.png"), "Povezana Tabela")

    def delete_tab(self, index):
        self.tab_widget.removeTab(index)

    def refresh_table(self, file_handler): 
        generic_model = GenericModel(self, file_handler.metadata)
        generic_model.elements = file_handler.get_all()
        self.file_handler = file_handler
        self.main_table.setModel(generic_model)

    def refresh_subtable(self, file_handler): 
        generic_model = GenericModel(self, file_handler.metadata)
        generic_model.elements = file_handler.get_all()
        self.subtable.setModel(generic_model)
    def save(self):
        self.file_handler.save()
    

    
    