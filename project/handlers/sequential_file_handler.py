from handlers.file_handler import FileHandler
import json
import csv


class SequentialFileHandler():
    def __init__(self, file_path, meta_filepath):
        super().__init__()
        self.file_path = file_path
        self.meta_filepath = meta_filepath
        self.data = []
        self.metadata = {}
        self.load_data()

        #Napraviti binarnu pretragu posebno
    def load_data(self):
        with open((self.file_path), 'r', newline="") as infile:
            reader = csv.reader(infile, delimiter=",", quotechar="'")
            for row in reader:
                self.data.append(row)
                print(row)

        with open(self.meta_filepath) as meta:
            self.metadata = json.load(meta)
         
    def get_all(self):
        #Dobavljanje svih elemenata 
        return self.data

    def get_one(self,id):
        #Dobavljanje jednog elementa, kljuc je unapred definisan
        for i in self.data:
            if getattr(i, (self.metadata["key"])) == id:
                return i
        return None


    def insert(self, obj): #treba resiti
        self.data.append(obj)
        with open((self.file_path), 'w', newline='') as csvfile:
            csv.writer(self.data,csvfile)
    

    def insert_many(self, objects):
        for obj in objects:
            self.data.append(obj)

    def save(self):
        with open((self.file_path), 'w', newline='') as csvfile:
            upis = csv.writer(csvfile)
            for i in self.data:
                upis.writerow(i)
                
    def delete_one(self, id):
        #Brisanje po id-u
        for i in self.data:
            if getattr(i, (self.metadata["key"])) == id:
                self.data.remove(i)