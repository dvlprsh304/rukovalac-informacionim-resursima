class TokStudija():
    def __init__(self, ustanova="", oznaka_programa="", student_iz_ustanove="", struka="", broj_indeksa=6, skolska_godina="", godina_studija="", blok="", redni_broj_upisa="", datum_upisa="", datum_overe="", espb_pocetni="", espb_krajnji=""):
        self.ustanova = ustanova
        self.oznaka_programa = oznaka_programa
        self.student_iz_ustanove = student_iz_ustanove
        self.struka = struka
        self.broj_indeksa = broj_indeksa
        self.skolska_godina = skolska_godina
        self.godina_studija = godina_studija
        self.blok = blok
        self.redni_broj_upisa = redni_broj_upisa
        self.datum_upisa = datum_upisa
        self.datum_overe = datum_overe
        self.espb_pocetni = espb_pocetni
        self.espb_krajnji = espb_krajnji

